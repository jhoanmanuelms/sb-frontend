/**
 * Definicion de la funcionalidad relacionada con el modulo de clientes.
 *
 * @author Jhoan Mu�oz
 */
(function(){
	/** Definicion del modulo de clientes. */
	var app = angular.module('clientes', []);
	
	/**
	 * Controlador utilizado para procesar peticiones relacionadas a los clientes.
	 */
	app.controller('controladorClientes', ['$http', function($http){
		
		/**
		 * Almacena el objeto actual para que pueda ser utilizado dentro del contexto de
		 * las llamadas http.
		 */
		var clienteCtrl = this;
		
		/** Modelo utilizado para almacenar la informacion del cliente. */
		this.cliente = {};
		
		/** Bandera utilizada para determinar el resultado de la consulta de un cliente. */
		this.cliente.registrado = true;
		
		/** URL utilizada para hacer peticiones al backend. */
		this.urlServer = 'http://localhost:8080';

		/**
		 * Realiza la peticion para registrar un cliente utilizando la informacion almacenada
		 * en el modelo de cliente.
		 */
		this.registrarCliente = function(){
			var req = {
				method: 'POST',
				url: this.urlServer + '/registroCliente',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					cedula: this.cliente.cedula,
					nombre: this.cliente.nombre,
					direccion: this.cliente.dir,
					telefono: this.cliente.tel
				}
			};
			$http(req).
				success(function(){
					bootbox.alert("Cliente Registrado Correctamente");
					clienteCtrl.cliente = {};
				}).
				error(function(){
					bootbox.alert("Ha Ocurrido Un Error");
				});
		};
		
		/**
		 * Realiza la peticion para consultar un cliente utilizando la informacion almacenada
		 * en el modelo de cliente.
		 */
		this.consultarCliente = function(){
			var req = {
				method: 'POST',
				url: this.urlServer + '/consultarCliente',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					cedula: this.cliente.cedula
				}
			};
			$http(req).
				success(function(data){
					clienteCtrl.cliente = data;
					clienteCtrl.cliente.registrado = true;
				}).
				error(function(data){
					console.log("Error al cargar los datos del cliente");
					clienteCtrl.cliente.registrado = false;
					clienteCtrl.cliente.nombre = "";
					clienteCtrl.cliente.direccion = "";
					clienteCtrl.cliente.telefono = "";
					clienteCtrl.cliente.cuentas = "";
				});
		};
		
		/**
		 * Realiza la peticion para actualizar un cliente utilizando la informacion almacenada
		 * en el modelo de cliente.
		 */
		this.editarCliente = function(){
			var req = {
				method: 'POST',
				url: this.urlServer + '/editarCliente',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					cedula: this.cliente.cedula,
					nombre: this.cliente.nombre,
					direccion: this.cliente.direccion,
					telefono: this.cliente.telefono
				}
			};
			$http(req).
				success(function(){
					bootbox.alert("Cliente Actualizado Correctamente");
					clienteCtrl.cliente = {};
				}).
				error(function(){
					bootbox.alert("Ha Ocurrido Un Error");
				});
		};
		
		/**
		 * Realiza la peticion para eliminar un cliente utilizando la informacion almacenada
		 * en el modelo de cliente.
		 */
		this.eliminarCliente = function(){
			if (this.cliente.cedula && this.cliente.nombre)
			{
				bootbox.confirm(
					"Esta seguro de eliminar el cliente?",
					function(result){
						if (result)
						{
							var req = {
								method: 'POST',
								url: clienteCtrl.urlServer + '/eliminarCliente',
								headers: {
									'Content-Type': 'application/json'
								},
								data: {
									cedula: clienteCtrl.cliente.cedula
								}
							};
							$http(req).
								success(function(){
									bootbox.alert("Cliente Eliminado Correctamente");
									clienteCtrl.cliente = {};
									clienteCtrl.cliente.registrado = true;
								}).
								error(function(){
									bootbox.alert("Ha Ocurrido Un Error");
								});
						}
					});
			}
		};
	}]);
})();