/**
 * Modulo principal de la aplicacion. Contiene las directivas utilizadas a nivel global
 * y las dependencias a los modulos.
 *
 * @author Jhoan Mu�oz
 */
(function(){

	/** Definicion del modulo principal de la aplicacion. */
	var app = angular.module('simu-bank', ['clientes', 'cuentas']);

	/** Directiva utilizada para desplegar la barra de navegacion de la aplicacion. */
	app.directive('navigationBar', function(){
		return {
			restrict: 'E',
			templateUrl: '../componentes/NavBar.html'
		};
	});
})();