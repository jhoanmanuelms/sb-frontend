/**
 * Definicion de la funcionalidad relacionada con el modulo de cuentas.
 *
 * @author Jhoan Mu�oz
 */
(function(){

	/** Definicion del modulo de clientes. */
	var app = angular.module('cuentas', []);
	
	/**
	 * Controlador utilizado para procesar peticiones relacionadas a las cuentas.
	 */
	app.controller('controladorCuentas', ['$http', '$scope', '$filter', function($http, $scope, $filter){
		
		/**
		 * Almacena el objeto actual para que pueda ser utilizado dentro del contexto de
		 * las llamadas http.
		 */
		var cuentaCtrl = this;
		
		/** Identificador para movimientos bancarios de tipo debito. */
		this.ID_MOVIMIENTO_DEBITO = 1;

		/** Identificador para movimientos bancarios de tipo credito. */
		this.ID_MOVIMIENTO_CREDITO = 2;
		
		/** Nombre para movimientos bancarios de tipo debito. */
		this.NOMBRE_MOVIMIENTO_DEBITO = 'Debito';
		
		/** Nombre para movimientos bancarios de tipo credito. */
		this.NOMBRE_MOVIMIENTO_CREDITO = 'Credito';

		/** Modelo utilizado para almacenar la informacion de la cuenta. */
		this.cuenta = {};
		
		/** Modelo utilizado para almacenar la informacion de los clientes. */
		this.clientes = {};
		
		/** Bandera utilizada para determinar el resultado de la consulta de una cuenta. */
		this.cuenta.registrada = true;
		
		/** URL utilizada para hacer peticiones al backend. */
		this.urlServer = 'http://localhost:8080';

		/**
		 * Realiza la peticion para cargar los clientes registrados en la aplicacion al
		 * momento de iniciar la vista de clientes.
		 */
		$scope.$watch('$viewContentLoaded', function() {
			cuentaCtrl.cargarClientes();
		});
		
		/**
		 * Realiza la peticion para consultar los clientes registrados y los almacena en el modelo
		 * de clientes para poder utilizar esta informacion en las vistas.
		 */
		this.cargarClientes = function() {
			var req = {
				method: 'POST',
				url: this.urlServer + '/consultarClientes',
				headers: {
					'Content-Type': 'application/json'
				}
			};
			$http(req).
				success(function(data){
					cuentaCtrl.clientes = data;
				}).
				error(function(){
					bootbox.alert("Ha Ocurrido Un Error Al Cargar Los Clientes");
				});
		};
		
		/**
		 * Realiza la peticion para registrar una cuenta utilizando la informacion almacenada
		 * en el modelo de cuenta.
		 */
		this.registrarCuenta = function(){
			var req = {
				method: 'POST',
				url: this.urlServer + '/registroCuenta',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					cedulaCliente: this.cuenta.cedula,
					saldo: this.cuenta.saldo,
				}
			};
			$http(req).
				success(function(){
					bootbox.alert("Cuenta Registrada Correctamente");
					cuentaCtrl.cuenta = {};
				}).
				error(function(){
					bootbox.alert("Ha Ocurrido Un Error");
				});
		};
		
		/**
		 * Realiza la peticion para consultar una cuenta utilizando la informacion almacenada
		 * en el modelo de cuenta.
		 */
		this.consultarCuenta = function(){
			var req = {
				method: 'POST',
				url: this.urlServer + '/consultarCuenta',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					numero: this.cuenta.numero
				}
			};
			$http(req).
				success(function(data){
					cuentaCtrl.cuenta = data;
					cuentaCtrl.cuenta.registrada = true;
					
					// Aplicar filtro de moneda al saldo de la cuenta
					cuentaCtrl.cuenta.saldo = $filter('currency')(cuentaCtrl.cuenta.saldo);
					
				}).
				error(function(data){
					console.log("Error al cargar los datos de la cuenta");
					cuentaCtrl.cuenta.registrada = false;
					cuentaCtrl.cuenta.estado = "";
					cuentaCtrl.cuenta.saldo = "";
					cuentaCtrl.cuenta.cedulaCliente = "";
					cuentaCtrl.cuenta.nombreCliente = "";
				});
		};
		
		/**
		 * Realiza la peticion para eliminar una cuenta utilizando la informacion almacenada
		 * en el modelo de cuenta.
		 */
		this.eliminarCuenta = function(){
			if (this.cuenta.numero && this.cuenta.estado)
			{
				bootbox.confirm(
					"Esta seguro de eliminar la cuenta?",
					function(result){
						if (result)
						{
							var req = {
								method: 'POST',
								url: cuentaCtrl.urlServer + '/eliminarCuenta',
								headers: {
									'Content-Type': 'application/json'
								},
								data: {
									numero: cuentaCtrl.cuenta.numero
								}
							};
							$http(req).
								success(function(){
									bootbox.alert("Cuenta Eliminada Correctamente");
									cuentaCtrl.cuenta = {};
									cuentaCtrl.cuenta.registrada = true;
								}).
								error(function(){
									bootbox.alert("Ha Ocurrido Un Error");
								});
						}
					});
			}
		};
		
		/**
		 * Abre el dialogo para registrar un movimiento utilizando la cuenta actual.
		 */
		this.dialogoRegistroMovimiento = function(){
			if (this.cuenta.numero && this.cuenta.estado && this.cuenta.estado == 'Activa')
			{
				$("#registroMovimientoDiv").dialog({modal: true, width: 600});
			}
			else
			{
				bootbox.alert("Debe seleccionar una cuenta ACTIVA para registrar un movimiento");
			}
		};
		
		/**
		 * Realiza la peticion para registrar un moviento bancario utilizando la informacion almacenada
		 * en los modelos de cuenta y movimiento.
		 */
		this.registrarMovimiento = function(){
			var saldoNumerico = Number(this.cuenta.saldo.substr(1, this.cuenta.saldo.length));

			if (this.movimiento.tipo == String(this.ID_MOVIMIENTO_CREDITO)
				&& (saldoNumerico - this.movimiento.valor) < 1)
			{
				bootbox.alert("La cuenta no posee fondos suficientes para esta transaccion");
				return false;
			}
			
			var req = {
				method: 'POST',
				url: this.urlServer + '/registrarMovimiento',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					valor: this.movimiento.valor,
					numeroCuenta: this.cuenta.numero,
					idTipoMovimiento: this.movimiento.tipo
				}
			};
			$http(req).
				success(function(data){
					bootbox.alert("Movimiento Registrado.");
					cuentaCtrl.movimiento.valor = "";
					cuentaCtrl.movimiento.tipo = "";
					cuentaCtrl.cuenta.saldo = $filter('currency')(data);
					
				}).
				error(function(){
					bootbox.alert("Ha ocurrido un error.");
				});
		};
		
		/**
		 * Abre el dialogo para generar el reporte de movimientos por fecha.
		 */
		this.dialogoReporteMovimientos = function(){
			if (this.cuenta.numero && this.cuenta.estado)
			{
				$("#fechaInicial").datepicker({dateFormat: "yy-mm-dd"});
				$("#fechaFinal").datepicker({dateFormat: "yy-mm-dd"});
				$("#reporteMovimientosDiv").dialog({modal: true, width: 600});
			}
			else
			{
				bootbox.alert("Debe seleccionar una cuenta ACTIVA para registrar un movimiento");
			}
		};
		
		/**
		 * Realiza la peticion para generar el reporte de movimientos registrados en la cuenta seleccionada
		 * durante las fechas especificadas.
		 */
		this.reporteMovimientos = function(){		
			var req = {
				method: 'POST',
				url: this.urlServer + '/reporteMovimientos',
				headers: {
					'Content-Type': 'application/json'
				},
				data: {
					numeroCuenta: this.cuenta.numero,
					fechaInicial: this.reporte.fechaInicial,
					fechaFinal: this.reporte.fechaFinal
				}
			};
			$http(req).
				success(function(data){
					cuentaCtrl.movimientos = data;
					$("#reporteMovimientosDiv").dialog('close');
					
				}).
				error(function(){
					bootbox.alert("Ha ocurrido un error.");
				});
		};
	}]);
})();